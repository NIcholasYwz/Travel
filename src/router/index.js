import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/pages/Home.vue'
import City from '@/pages/city/City.vue'
import Detail from '@/pages/Detail/detail'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/', component: Home
    }, {
      path: '/city', component: City
    }, {
      path: '/detail/:id', component: Detail
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})
